package me.sinu.vottee.domain;

import lombok.Getter;

import java.util.EnumSet;
import java.util.Optional;

/**
 * Created by SiJohn on 3/29/2015.
 */
public enum FilterType {
    GENDER(DomainConstants.GENDER),
    PROFESSION(DomainConstants.PROFESSION);

    @Getter
    private String filter;

    private FilterType(String filter) {
        this.filter = filter;
    }

    public static FilterType get(String filter) {
        final Optional<FilterType> filterOptional = EnumSet.allOf(FilterType.class).stream().filter(filterType -> filterType.getFilter().equals(filter)).findFirst();
        if(filterOptional.isPresent()) {
            return filterOptional.get();
        }
        return null;
    }
}
