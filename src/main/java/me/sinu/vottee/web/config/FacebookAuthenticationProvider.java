package me.sinu.vottee.web.config;

import me.sinu.vottee.web.dao.UserCredentialDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by SiJohn on 3/14/2015.
 */
@Component
public class FacebookAuthenticationProvider implements AuthenticationProvider, Serializable {

    @Autowired
    private UserCredentialDao userCredentialDao;

    public Authentication authenticate(Authentication authentication)
            throws AuthenticationException {

        String username = (String)authentication.getPrincipal();
        String password = (String)authentication.getCredentials();
        User user = authenticate(username, password);
        if(user != null) {
            return new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
        } else {
            throw new AuthenticationException("Wrong USER!"){
                private static final long serialVersionUID = 8522046256394736611L;};
        }
    }

    private User authenticate(String username, String password) {
        //TODO: call FB API and validate. Return null if not valid
        List<GrantedAuthority> authorities = new ArrayList<>(1);
        authorities.add(new VotteeUserAuthority());
        final Map<String, Object> userMap = userCredentialDao.getUserCredential(username);
        final String uid = (String)userMap.get("uid");
        return new VotteeUserDetails(username, "", authorities, uid);
    }

    public boolean supports(Class<?> authentication) {
        return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
    }
}

class VotteeUserAuthority implements GrantedAuthority{

    private static final long serialVersionUID = -999194729716487662L;

    @Override
    public String getAuthority() {
        return "ROLE_USER";
    }
}
