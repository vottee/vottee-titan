package me.sinu.vottee.web.dao;

import com.tinkerpop.gremlin.structure.Edge;
import com.tinkerpop.gremlin.structure.Element;
import com.tinkerpop.gremlin.structure.Vertex;

import java.util.List;

/**
 * Created by SiJohn on 3/26/2015.
 */
public interface GraphRepository {

    Vertex addVertex(String label);

    Vertex getVertex(Object id, String label);

    Object getPropertyIfExists(Element element, String key);

    Edge addUnidirectionalEdge(Vertex start, Vertex end, String label);

    void removeUnidirectionalEdge(Vertex start, Vertex end, String label);

    Edge getDirectionalEdge(Vertex start, Vertex end, String label);

    List<Vertex> getOutgoingVertices(Vertex start, String label, long low, long high);

    Long getOutgoingVerticesCount(Vertex start, String label);

    List<Vertex> getIncomingVertices(Vertex start, String label, long low, long high);

    Long getIncomingVerticesCount(Vertex start, String label);

    Edge getSingleIncomingEdge(Vertex vertex, String label);
}
