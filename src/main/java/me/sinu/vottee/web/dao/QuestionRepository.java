package me.sinu.vottee.web.dao;

import com.tinkerpop.gremlin.structure.Vertex;
import me.sinu.vottee.domain.FilterType;
import me.sinu.vottee.web.payload.LikesPayload;
import me.sinu.vottee.web.payload.QuestionPayload;
import me.sinu.vottee.web.payload.UserPayload;

import java.util.List;
import java.util.Map;

/**
 * Created by SiJohn on 3/26/2015.
 */
public interface QuestionRepository {
    Vertex addQuestion();

    Vertex getQuestion(Object id);

    void removeQuestion(Object id);

    String getDescription(Vertex vertex);

    void setDescription(Vertex vertex, String description);

    List<String> getAnswers(Vertex vertex);

    void setAnswers(Vertex vertex, Object[] answers);

    Integer getQuestionType(Vertex vertex);

    void setQuestionType(Vertex vertex, int questionType);

    Long getExpiry(Vertex vertex);

    void setExpiry(Vertex vertex, long expiry);

    Boolean getHideResults(Vertex vertex);

    void setHideResults(Vertex vertex, boolean hideResults);

    Long getCreatedTime(Vertex vertex);

    List<UserPayload> getAnsweredUsers(Vertex question, long low, long high);

    Long getAnsweredUsersCount(Vertex question);

    LikesPayload getLikes(Vertex question);

    Object getCreatedUser(Vertex question);

    Map<String, Object> getVoteResult(Vertex question, List<FilterType> filters);

    QuestionPayload getQuestion(Vertex question);
}
