package me.sinu.vottee.web.resources;

import me.sinu.vottee.web.payload.AnswerPayload;
import me.sinu.vottee.web.payload.QuestionPayload;
import me.sinu.vottee.web.payload.IdPayload;

import javax.ws.rs.*;
import java.util.Map;

/**
 * Created by SiJohn on 3/8/2015.
 */
@Path("/question")
public interface QuestionService {

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Path("/create")
    IdPayload createQuestion(QuestionPayload questionPayload);

    @GET
    @Produces("application/json")
    @Path("/{qid}")
    QuestionPayload getQuestion(@PathParam("qid") String questionId);

    @POST
    @Path("/{qid}/answer")
    void answerQuestion(@PathParam("qid") String questionId, AnswerPayload answerPayload);

    @POST
    @Path("/{qid}/up")
    void upVoteQuestion(@PathParam("qid") String questionId);

    @POST
    @Path("/{qid}/down")
    void downVoteQuestion(@PathParam("qid") String questionId);

    @GET
    @Path("/{qid}/result/{filters: .*}")
    Map<String, Object> getVoteResult(@PathParam("qid") String questionId, @PathParam("filters") String filters);

}
