package me.sinu.vottee.web.payload;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlElement;

/**
 * Created by SiJohn on 3/8/2015.
 */
public class UserPayload {
    @XmlElement(name = "name")
    @Getter
    @Setter
    private String name;

    @XmlElement(name = "id")
    @Getter
    @Setter
    private String id;

}
