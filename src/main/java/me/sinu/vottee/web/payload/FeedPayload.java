package me.sinu.vottee.web.payload;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlElement;
import java.util.List;

/**
 * Created by ARawat on 16-Apr-15.
 */
public class FeedPayload{

    @XmlElement(name = "questions")
    @Getter
    @Setter
    private List<QuestionPayload> questions;
}
